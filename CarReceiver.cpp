#include "BLEDevice.h"
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include "mbedtls/aes.h"

// The remote service we wish to connect to.
static BLEUUID serviceUUID("4fafc201-1fb5-459e-8fcc-c5c9c331914b");
// The characteristic of the remote service we are interested in.
static BLEUUID charUUID("beb5483e-36e1-4688-b7f5-ea07361b26a8");
static boolean doConnect = false;
static boolean connected = false;
static boolean doScan = false;
static BLERemoteCharacteristic *pRemoteCharacteristic;
static BLEAdvertisedDevice *myDevice;
int OpenIter = 0;
int CloseIter = 0;
static boolean currentState = false; // if false- closed lock, if true- open lock
float myRssi = -1000;
#define uS_TO_S_FACTOR 1000000
#define TIME_TO_SLEEP 10
unsigned long time1, time2;
char desiredPassword[16];
char receivedPassword[17] = {0};
char plainText[17] = {0};
char *key = "2Z7DJqr9cd8QlbJS"; //definition of a private key
unsigned char decipheredTextOutput[16];
std::string CharVal;
String message;
bool ReadWriteMode = false;
int disconnectIter = 0;
bool sleepDisable = false;
bool keySleep = false;

class MyClientCallback : public BLEClientCallbacks
{
    void onConnect(BLEClient *pclient)
    {
        connected = true;
        disconnectIter = 0;
        digitalWrite(2, HIGH);
    }

    void onDisconnect(BLEClient *pclient)
    {
        connected = false;
        digitalWrite(2, LOW);
        digitalWrite(12, LOW);
        sleepDisable = false;
        disconnectIter++;
        if (CloseIter == 1 && currentState == true)
        {
            Serial.println("The key suddenly disconnected");
            Serial.println("Close the locks");
            digitalWrite(26, LOW); // Turn on the lock actuator
            delay(500);
            digitalWrite(26, HIGH); // Turn off the lock actuator
            currentState = false;
        }
        esp_deep_sleep_start();
    }
};

static void notifyCallback(BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify)
{
    Serial.print("Notify callback for characteristic ");
    Serial.print(pBLERemoteCharacteristic->getUUID().toString().c_str());
    Serial.print(" of data length ");
    Serial.println(length);
    Serial.print("data: ");
    Serial.println((char *)pData);
}

bool connectToServer()
{
    if (myRssi > -95) // -69 is 1m, -75 is 2m, -83 is 5m, -89 is 10m, -95 is 20m
    {                 // If the key is in range of 20m -> wake up keyless access system
        sleepDisable = true;
        Serial.println("SleepDisabled");
        digitalWrite(12, HIGH); // Wake up arduino
        digitalWrite(25, HIGH); // Arduino disable falling asleep
        Serial.println("wake up arduino");
        if (myRssi > -83 && !keySleep)
        {                  // If the key is closer than 5m from the car
            OpenIter++;    // Variable for opening just once
            CloseIter = 0; // Variable for closing just once
            Serial.print("Forming a connection to ");
            Serial.println(myDevice->getAddress().toString().c_str());
            BLEClient *pClient = BLEDevice::createClient();                      // Create BLE client device
            pClient->setClientCallbacks(new MyClientCallback());                 // Create new callbacks corressponding to the just created client
            pClient->connect(myDevice);                                          // Try to connect
            BLERemoteService *pRemoteService = pClient->getService(serviceUUID); // try to find a server with the desired service UUID
            if (pRemoteService == nullptr)
            {
                Serial.print("Failed to find our service UUID: ");
                Serial.println(serviceUUID.toString().c_str());
                pClient->disconnect();
                return false;
            }
            // server with desired service UUID found
            // now to ensure connecting to the right device, check if the service advertises with desired characteristic UUID
            pRemoteCharacteristic = pRemoteService->getCharacteristic(charUUID);
            if (pRemoteCharacteristic == nullptr)
            {
                Serial.print("Failed to find our characteristic UUID: ");
                Serial.println(charUUID.toString().c_str());
                pClient->disconnect();
                return false;
            }
            if (pRemoteCharacteristic->canRead())
            {
                delay(500);
                std::string value = pRemoteCharacteristic->readValue();
            }
            if (pRemoteCharacteristic->canNotify())
            {
                pRemoteCharacteristic->registerForNotify(notifyCallback);
            }
            connected = true;
            return true;
        }
    }
    if (myRssi <= -95)
    {                          // Not in range of 20m
        digitalWrite(12, LOW); // Let Arduino go to sleep
        Serial.println("Too low RSSI");
        sleepDisable = false;
        connected = false;
        return false;
    }
}

void decrypt(unsigned char *chipherText, char *key, unsigned char *outputBuffer)
{
    mbedtls_aes_context aes;
    mbedtls_aes_init(&aes);
    mbedtls_aes_setkey_dec(&aes, (const unsigned char *)key, strlen(key) * 16);
    mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_DECRYPT, (const unsigned char *)chipherText, outputBuffer);
    mbedtls_aes_free(&aes);
}

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks
{
    void onResult(BLEAdvertisedDevice advertisedDevice)
    {
        Serial.print("BLE Advertised Device found: ");
        Serial.println(advertisedDevice.toString().c_str());
        myRssi = float(advertisedDevice.getRSSI());
        distance = pow(10, ((-69 - float(advertisedDevice.getRSSI())) / (20)));
        if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID))
        { // if found the desired device then calculate its distance from the car receiver
            Serial.print("BLE Advertised Device found: ");
            Serial.println(advertisedDevice.toString().c_str());
            myRssi = float(advertisedDevice.getRSSI());
            distance = pow(10, ((-69 - float(advertisedDevice.getRSSI())) / (20)));
            Serial.print("RSSI: ");
            Serial.println(advertisedDevice.getRSSI());
            Serial.print("Distance: ");
            Serial.println(distance);
            if (myRssi > -95)
            { // if closer than ~20m
                sleepDisable = true;
                Serial.println("Sleep disabled");
            }
            else
            {
                sleepDisable = false;
            }
            BLEDevice::getScan()->stop();
            myDevice = new BLEAdvertisedDevice(advertisedDevice);
            doConnect = true;
            doScan = true;
        }
    }
};

void confirmConnection()
{
    if (doConnect == true)
    {
        if (connectToServer())
        {
            Serial.println("We are now connected to the BLE Server.");
        }
        else
        {
            Serial.println("We have failed to connect to the server");
            connected = false;
            BLEScan *pBLEScan = BLEDevice::getScan(); // Search for the key again
            pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
            pBLEScan->setInterval(1349);
            pBLEScan->setWindow(449);
            pBLEScan->setActiveScan(true);
            pBLEScan->start(5, false);
        }
        doConnect = false;
    }
}

void checkKeyDistance()
{
    BLEDevice::deinit();
    Serial.println("restarting client...\n");
    connected = false;
    BLEDevice::init("ESP32-BLE-Client");
    BLEScan *pBLEScan = BLEDevice::getScan();
    pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
    pBLEScan->setInterval(1349);
    pBLEScan->setWindow(449);
    pBLEScan->setActiveScan(true);
    pBLEScan->start(5, false);
}

void randomPlainTextGenerator(char *output, int len)
{ // Generate a random text, wich will be encrypted and serve as a password
    char *allowedChars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
    for (int i = 0; i < len; i++)
    {
        int randomIndex = random(0, strlen(allowedChars));
        output[i] = allowedChars[randomIndex];
    }
}

void askForPassword()
{ // If previous conditions were met then this function is called before opening the lock
    randomPlainTextGenerator(plainText, 16);
    Serial.println("Generated a password");
    for (int i = 0; i < 16; i++)
    {
        desiredPassword[i] = plainText[i];
    }
    CharVal = pRemoteCharacteristic->readValue();
    message = CharVal.c_str();
    if (message == "Hello, World!" && !ReadWriteMode)
    {                         // If message from the BLE server is "Hello, World!" it indicates that the server is ready to start new authentication procedure
        ReadWriteMode = true; // Change to write mode
        // change characteristic value to plainText
        pRemoteCharacteristic->writeValue(plainText, 16);
        CharVal = pRemoteCharacteristic->readValue();
        message = CharVal.c_str();
        // save the characteristic value to check if it changed
    }
    time1 = millis(); // Start the timer for the password active time
    time2 = millis();
    while (message == String(plainText) && time2 - time1 < 5000)
    { // read the characteristic value till it's changed
        CharVal = pRemoteCharacteristic->readValue();
        message = CharVal.c_str();
        delay(200);
        time2 = millis();
    }
    time1 = 0; // reset the timer
    time2 = 0;
    if (message != "Hello, World!" && message != String(plainText) && ReadWriteMode)
    {                          // if the characteristic value was changed then check if it was encrypted within our encryption system
        ReadWriteMode = false; // change to read mode
        char received[message.length()];
        message.toCharArray(received, message.length());
        decrypt((unsigned char *)received, key, decipheredTextOutput);
        Serial.print("Desired password:      ");
        Serial.println(desiredPassword);
        Serial.println("\nEncrypted message: ");
        for (int i = 0; i < 16; i++)
        {
            char str[3];
            sprintf(str, "%02x", (int)decipheredTextOutput[i]);
            Serial.print(str);
        }
        for (int i = 0; i < 16; i++)
        {
            receivedPassword[i] = (char)decipheredTextOutput[i];
        }
        Serial.print("\nDecrypted message:   ");
        Serial.println(receivedPassword);
        pRemoteCharacteristic->writeValue("Hello, World!");
        // reset the characteristic value
    }
}

void communicationHandler()
{
    if (connected)
    {
        Serial.println("Connected");
        CharVal = pRemoteCharacteristic->readValue();
        message = CharVal.c_str();
        if (message == "SLEEP")
        {
            keySleep = true;
            Serial.println("Key informed that it will go to sleep");
        }
        else
        {
            keySleep = false;
        }
        if (OpenIter == 1 && currentState == false /*If locks closed*/)
        {
            askForPassword(); // Ask the key for a password
            if (String(receivedPassword) == String(desiredPassword))
            {
                Serial.println("Password's correct - open the locks");
                OpenIter++;
                CloseIter = 0;
                digitalWrite(27, LOW); // TURN ON THE ACTUAtOR
                delay(500);
                digitalWrite(27, HIGH); // TURN OFF THE ACTUATOR
                currentState = true;
            }
            else
            {
                Serial.println("The password is wrong, going to sleep");
                esp_deep_sleep_start();
                Serial.print("NOT PRINTED");
            }
        }
        // The following part deals with checking if the server is still in a desired range
        // It resets the client & connection, and then the function connectToServer() deals with the connection
        delay(2000);
        checkKeyDistance();
        if (!keySleep)
        {
            if (myRssi < -83)
            {
                CloseIter++;
                OpenIter = 0;
                if (CloseIter == 1 && currentState == true)
                {
                    Serial.println("The key went away from the safe range");
                    Serial.println("Close the locks");
                    digitalWrite(26, LOW); // Turn on the Actuator
                    delay(500);
                    digitalWrite(26, HIGH); // Turn off the actuator
                    currentState = false;
                }
            }
        }
        else
        {
            Serial.println("key is sleeping while checking distance");
        }
    }
    else if (!connected) // NOT CONNECTED
    {//if the connection state is false and the key did not inform about going to sleep it indicates 
     //that the key went outside the safe area and we need to close the locks
        if (!keySleep)
        {
            if (CloseIter == 0 && currentState == true || disconnectIter >= 1 && currentState)
            {
                Serial.println("Close the locks");
                OpenIter = 0;
                CloseIter++;
                digitalWrite(26, LOW); // Turn on the Actuator
                delay(500);
                digitalWrite(26, HIGH); // Turn off the actuator
                currentState = false;
                digitalWrite(2, LOW);
            }
        }
        else
        {
            Serial.println("The key is sleeping, let's go to sleep");
            digitalWrite(25, LOW); // Arduino to sleep
            delay(500);
            digitalWrite(25, HIGH);
            esp_deep_sleep_start();
            Serial.print("NOT PRINTED");
        }
        delay(1000);
        if (!sleepDisable)
        {
            Serial.println("Didn't find the key - going to sleep for 10s");
            digitalWrite(25, LOW); // Arduino to sleep
            delay(500);
            digitalWrite(25, HIGH);
            esp_deep_sleep_start();
            Serial.print("NOT PRINTED");
        }
        else if (sleepDisable || sleepDisable && !keySleep || disconnectIter >= 1)
        {
            Serial.println("Sleep disabled - not going to sleep");
            checkKeyDistance();
        }
    }
}
void setup()
{
    pinMode(2, OUTPUT);  // built in led for connection indicating
    pinMode(12, OUTPUT); // WAKE UP ARDUINO
    pinMode(25, OUTPUT); // ARDUINO GO TO SLEEP
    pinMode(26, OUTPUT); // CLOSE THE LOCKS
    pinMode(27, OUTPUT); // OPEN THE LOCKS
    digitalWrite(26, HIGH);
    digitalWrite(27, HIGH);
    digitalWrite(12, LOW);
    randomSeed(analogRead(4));
    esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
    Serial.begin(115200);
    Serial.println("Starting Arduino BLE Client application...");
    BLEDevice::init("ESP32-BLE-Client");
    BLEScan *pBLEScan = BLEDevice::getScan();
    pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
    pBLEScan->setInterval(1349);
    pBLEScan->setWindow(449);
    pBLEScan->setActiveScan(true);
    pBLEScan->start(5, false);
    delay(1000);
}

void loop()
{
    confirmConnection();
    communicationHandler();
    delay(1000);
}