#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>
#include <BLEAdvertisedDevice.h>
#include "mbedtls/aes.h"
//defining a unique identifier for our device
#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
//Defining a unique characteristic identifier of our service
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
BLEServer *pServer;
BLEService *pService;
BLECharacteristic *pCharacteristic;
static boolean doAdvert = false;
char charMessage[17] = {0};
char *key = "2Z7DJqr9cd8QlbJS";//definition of a private key
bool connected = false;
int time1 = 0, time2 = 0;
std::string value;
String message;

class MyServerCallbacks : public BLEServerCallbacks
{
    void onConnect(BLEServer *pServer)
    {
        connected = true;
    }
    void onDisconnect(BLEServer *pServer)
    {
        connected = false;
    }
};

void encrypt(char *plainText, char *key, unsigned char *outputBuffer)
{
    mbedtls_aes_context aes;
    mbedtls_aes_init(&aes);
    mbedtls_aes_setkey_enc(&aes, (const unsigned char *)key, strlen(key) * 16);
    mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_ENCRYPT, (const unsigned char *)plainText, outputBuffer);
    mbedtls_aes_free(&aes);
}

void communicationHandler()
{//based on the characteristic value our device can enter the authentication procedure
 //to start the characteristic value must be changed by the BLE client, so that it is not the default "Hello, World!"
    value = pCharacteristic->getValue();
    message = value.c_str();
    message.toCharArray(charMessage, 17);
    Serial.print("The charaterisitc value is   ");
    Serial.println(message);
    if (message != "Hello, World!")
    {
        delay(100);
        unsigned char output[16];
        encrypt(charMessage, key, output);//Encrypt the message
        Serial.println("\nCiphered text:");
        for (int i = 0; i < 16; i++)
        {
            char str[3];
            sprintf(str, "%02x", (int)output[i]);
            Serial.print(str);
        }
        Serial.println("\n");
        pCharacteristic->setValue((char *)output);//Send encrypted message to the client

        /*Verification part
        value = pCharacteristic->getValue();
        message = value.c_str();
        char charTest[message.length()];
        message.toCharArray(charTest, message.length());
        for (int i = 0; i < 16; i++)
        {
            char str[3];
            sprintf(str, "%02x", (int)charTest[i]);
            Serial.print(str);
        }
        Serial.println();*/
        delay(1000);
    }
}

void setup()
{
    esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, 1);
    time1 = millis();
    connected = false;
    Serial.begin(115200);
    Serial.println("Starting BLE Server!");

    BLEDevice::init("ESP32-BLE-Server");
    pServer = BLEDevice::createServer();
    pService = pServer->createService(SERVICE_UUID);
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        BLECharacteristic::PROPERTY_READ |
            BLECharacteristic::PROPERTY_WRITE
            );
    pCharacteristic->setValue("Hello, World!");
    pServer->setCallbacks(new MyServerCallbacks());
    pService->start();
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x06);
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();
}

void loop()
{
    if (connected)
    {
        if(time2 - time1 < 55000){//for testing purposes the time limit has been increased
            communicationHandler();
        }else{//if didn't manage to perform the authentication in time then prepare to go to sleep
            pCharacteristic->setValue("SLEEP");
        }
    }
    if (!connected)
    {
        BLEDevice::startAdvertising();
        Serial.println("Waiting for connection");
    }
    time2 = millis();
    if (time2 - time1 > 60000) //if time since wakeup is greater than the threshold value then go to sleep
    {
        Serial.println("time is up - go to sleep");
        esp_deep_sleep_start();
        Serial.println("NOT PRINTED");
    }
    delay(1000);
}