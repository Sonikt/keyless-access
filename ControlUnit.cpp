#include "LowPower.h"

int iter = 0;
int auxiter = 0;
bool turn = false;
unsigned long time1, time2;
int sleepIter = 0;
void wakeUp()
{
    detachInterrupt(0);
    Serial.println("Wake up");
}

void setup()
{
    pinMode(4, INPUT_PULLUP);  // CLOSE input signal from ESP
    pinMode(5, INPUT_PULLUP);  // OPEN input signal from ESP
    pinMode(6, INPUT_PULLUP);  // Go to sleep input signal
    pinMode(9, OUTPUT);        // Steering signal S1 for relay1
    pinMode(10, OUTPUT);       // Steering signal S2 for relay1
    pinMode(11, OUTPUT);       // Steering signal S1 for relay2
    pinMode(12, OUTPUT);       // Steering signal S2 for relay2
    digitalWrite(10, HIGH);
    digitalWrite(11, HIGH);
    digitalWrite(12, HIGH);
    digitalWrite(9, HIGH); // the relays are turned on by swiching s1 or s2 to LOW state
    // Setting at the start the pins S1 and S2 to HIGH will prevent any undesired turning on the relays
    Serial.begin(9600);
    delay(500);
}

void loop()
{
    if (digitalRead(5) == LOW && digitalRead(4) == HIGH)
    {
        iter++;
        if (iter == 15)//Assuring that the request is not triggered by the floating voltage
        {
            Serial.println("CLOSE");
            auxiter = 0;
            turn = true;
            digitalWrite(10, LOW);
            digitalWrite(11, HIGH);
            digitalWrite(12, LOW);
            digitalWrite(9, HIGH);
            delay(100);
            time1 = 0;
            time2 = 0;
        }
        else
        {
            digitalWrite(10, HIGH);
            digitalWrite(11, HIGH);
            digitalWrite(12, HIGH);
            digitalWrite(9, HIGH);
        }
    }
    else if (digitalRead(4) == LOW && digitalRead(5) == HIGH)
    {
        auxiter++;
        if (auxiter == 15)
        {
            Serial.println("OPEN");//Assuring that the request is not triggered by the floating voltage
            iter = 0;
            turn = false;
            digitalWrite(10, HIGH);
            digitalWrite(11, LOW);
            digitalWrite(12, HIGH);
            digitalWrite(9, LOW);
            delay(100);
        }
    }
    else
    {//the systen stays in rest
        iter = 0;
        auxiter = 0;
        digitalWrite(10, HIGH);
        digitalWrite(11, HIGH);
        digitalWrite(12, HIGH);
        digitalWrite(9, HIGH);
    }
    if (digitalRead(6) == LOW)//Signal to go to sleep
    {
        sleepIter++;
        if (sleepIter == 15)
        {
            // Allow wake up pin to trigger interrupt on a change of the state from LOW to HIGH.
            attachInterrupt(digitalPinToInterrupt(3), wakeUp, RISING);
            Serial.println("Going to sleep!!!");
            delay(1000);
            // Enter power down state with ADC and BOD module disabled.
            LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
            //When the interrupt occurs the device will continue from this point
        }
    }
    else
    {
        sleepIter = 0;
    }
}